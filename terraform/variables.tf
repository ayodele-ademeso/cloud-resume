variable "aws-region" {
  description = "AWS region to create resources"
  type        = string
}

variable "dynamo_db_table" {
  description = "Name of the DynamoDB Table"
  type        = string
}

variable "bucket_name" {
  description = "S3 bucket to store static files"
  type        = string
}

variable "acl_value" {
  description = "ACL to be set on bucket"
  type        = string
}

variable "website_domain" {
  description = "Name of the website. should be the same as the bucket name"
  type        = string
}

variable "lambda_function_name" {
  description = "Name of the lambda function to create"
  type        = string
}